#include "abillboard.h"

#include "amaterial.h"
#include "ageometry.h"
#include "avertexattribute.h"
#include "asimpletexturevertex.h"

ABillboard::ABillboard( const QString& textureFile )
	: AObject3D( false, false )
{
	prepareGeometry();
	prepareMaterial( textureFile );
}

ABillboard::~ABillboard()
{
}

void ABillboard::prepareMaterial( const QString& textureFile )
{
	AMaterial* material = new AMaterial();
	material->setShaders( "shaders/textured_object.vert", "shaders/textured_object.frag" );
	//material->setShaders( "shaders/test.vert", "shaders/test.frag" );

	ASampler* sampler = new ASampler();
    sampler->create();
    sampler->setMinificationFilter( GL_LINEAR_MIPMAP_LINEAR );
    sampler->setSamplerParameterF( GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f );
    sampler->setMagnificationFilter( GL_LINEAR );
	sampler->setWrapMode( ASampler::DirectionS, GL_CLAMP_TO_EDGE );
    sampler->setWrapMode( ASampler::DirectionT, GL_CLAMP_TO_EDGE );

	QImage image( textureFile );
    ATexture* texture = new ATexture();
    texture->create();
    texture->bind();
    texture->setImage( image );
    texture->generateMipMaps();
    material->setTextureUnit( 1, texture, sampler, QByteArrayLiteral( "texture0" ) );

	setMaterial( material );
}

void ABillboard::prepareGeometry()
{
	AGeometry* geometry = new AGeometry();
	geometry->setUsagePattern( QOpenGLBuffer::StaticDraw, QOpenGLBuffer::VertexBuffer );
	geometry->setUsagePattern( QOpenGLBuffer::StaticDraw, QOpenGLBuffer::IndexBuffer );
	
	ASimpleTextureVertex* vertices = new ASimpleTextureVertex[ 4 ];
	vertices[ 0 ].position = QVector3D( -1.0f, 1.0f, 0.0f );
	vertices[ 1 ].position = QVector3D( 1.0f, 1.0f, 0.0f );
	vertices[ 2 ].position = QVector3D( 1.0f, -1.0f, 0.0f );
	vertices[ 3 ].position = QVector3D( -1.0f, -1.0f, 0.0f );

	vertices[ 0 ].texCoords = QVector2D( 0.0f, 1.0f );
	vertices[ 1 ].texCoords = QVector2D( 1.0f, 1.0f );
	vertices[ 2 ].texCoords = QVector2D( 1.0f, 0.0f );
	vertices[ 3 ].texCoords = QVector2D( 0.0f, 0.0f );
	
	GLuint* indices = new GLuint[ 6 ];
	indices[ 0 ]	= 0;
	indices[ 1 ]	= 1;
	indices[ 2 ]	= 2;
	indices[ 3 ]	= 0;
	indices[ 4 ]	= 2;
	indices[ 5 ]	= 3;

	vertices[ 0 ].normal = QVector3D( 0, 0, 1.0f );
	vertices[ 1 ].normal = QVector3D( 0, 0, 1.0f );
	vertices[ 2 ].normal = QVector3D( 0, 0, 1.0f );
	vertices[ 3 ].normal = QVector3D( 0, 0, 1.0f );

	///AGeometry::calculateNormals( vertices, 4, indices, 6 );

	geometry->setVertices( vertices, 4 );
	geometry->setIndices( indices, 6 );

	AVertexAttribute* positionAttribute = new AVertexAttribute(
		QByteArrayLiteral( "positionAttribute" ), 
		GL_FLOAT, 0, 
		3, vertices->size() );

	AVertexAttribute* normalAttribute = new AVertexAttribute(
		QByteArrayLiteral( "normalAttribute" ), 
		GL_FLOAT, sizeof( QVector3D ), 
		3, vertices->size() );

	AVertexAttribute* textureCoordinateAttribute = new AVertexAttribute(
		QByteArrayLiteral( "textureCoordinateAttribute" ), 
		GL_FLOAT, 2 * sizeof( QVector3D ), 
		3, vertices->size() );

	geometry->setVertexAttribute( positionAttribute );
	geometry->setVertexAttribute( normalAttribute );
	geometry->setVertexAttribute( textureCoordinateAttribute );

	setGeometry( geometry );
}
