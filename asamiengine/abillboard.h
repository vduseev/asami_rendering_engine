#ifndef ABILLBOARD_H
#define ABILLBOARD_H

#include "aobject3D.h"

#include <QString>

class ABillboard : public AObject3D
{
public:
	ABillboard( const QString& textureFile );
	~ABillboard();

private:
	void prepareGeometry();
	void prepareMaterial( const QString& textureFile );
};

#endif

