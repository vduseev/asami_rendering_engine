#ifndef ATEXTUREDCUBE_H
#define ATEXTUREDCUBE_H

#include "aobject3D.h"

#include <QString>

class ATexturedCube : public AObject3D
{
public:
	ATexturedCube();

	void update( float time );

	void initialize( const QString& textureFile, bool inverseNormals = false );

private:
	void prepareMaterial( const QString& textureFile );
	void prepareGeometry( bool inverseNormals );
};

#endif

