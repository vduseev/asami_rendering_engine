#ifndef APOSTEXTNORMVERTEX_H
#define APOSTEXTNORMVERTEX_H

#include "aabstractvertex.h"

#include <QVector3D>
#include <QVector2D>
#include <qopengl.h>

class APosColorNormVertex : public AAbstractVertex
{
public:
	QVector3D position;
	QVector3D color;
	QVector3D normal;
};

#endif
