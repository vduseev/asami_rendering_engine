#include "acube.h"

#include "amaterial.h"
#include "ageometry.h"
#include "avertexattribute.h"
#include "aposcolornormvertex.h"

ACube::ACube(void)
{
	m_isLightable = true;
}

void ACube::initialize()
{
	prepareGeometry();
	prepareMaterial();

	AObject3D::initialize();
}

void ACube::prepareMaterial()
{
	AMaterial* material = new AMaterial();
	material->setShaders( "shaders/cube.vert", "shaders/cube.frag" );
	setMaterial( material );
}

void ACube::prepareGeometry()
{
	AGeometry* cube = new AGeometry();
	
	cube->initialize();
	cube->setUsagePattern( QOpenGLBuffer::StaticDraw, QOpenGLBuffer::VertexBuffer );
	cube->setUsagePattern( QOpenGLBuffer::StaticDraw, QOpenGLBuffer::IndexBuffer );

	APosColorNormVertex vertices[ 8 ];
	for ( int i = 0; i < 8; i++ )
	{
		// each even set of 4 vertices has -0.5f z-coord
		float z = ( ( i / 4 ) % 2 == 0 ) ? -0.5f : 0.5f;
		// each even pair of vertices has 0.5f y-coord;
		float y = ( ( i / 2 ) % 2 == 0 ) ? 0.5f : -0.5f; 
		// each vertex with index 0 or 3 has -0.5f x-coord;
		float x = ( ( i % 4 == 0 ) || ( ( i - 3 ) % 4 == 0 ) ) ? -0.5f : 0.5f;

		vertices[i].position = QVector3D( x, y, z );
		vertices[i].color = QVector3D( 0.5f + x, 0.5f + y, 0.5f + z );
	}
			
	GLuint indices[] = {
		0, 1, 2,	0, 2, 3,
		1, 5, 6,	1, 6, 2,
		5, 4, 7,	5, 7, 6,
		4, 0, 3,	4, 3, 7,
		4, 5, 1,	4, 1, 0,
		3, 2, 6,	3, 6, 7
	};

	for ( int i = 0; i < 36; i += 3 )
	{
		int index0 = indices[ i ],
			index1 = indices[ i + 1 ],
			index2 = indices[ i + 2 ];
		QVector3D v1 = vertices[ index1 ].position - vertices[ index0 ].position;
		QVector3D v2 = vertices[ index2 ].position - vertices[ index0 ].position;
		QVector3D polygonNormal = QVector3D::normal( v1, v2 );

		vertices[ index0 ].normal += polygonNormal;
		vertices[ index1 ].normal += polygonNormal;
		vertices[ index2 ].normal += polygonNormal;
	}

	for ( int i = 0; i < 8; i++ )
	{
		vertices[ i ].normal.normalize();
	}
	
	cube->setVertices( vertices, 3 * sizeof ( QVector3D ), 8 );
	cube->setIndices( indices, 36 );

	AVertexAttribute* positionAttribute = new AVertexAttribute(
		QByteArrayLiteral( "positionAttr" ), GL_FLOAT, 0, 3, 3 * sizeof( QVector3D ) );
	AVertexAttribute* colorAttribute = new AVertexAttribute(
		QByteArrayLiteral( "colorAttr" ), GL_FLOAT, sizeof( QVector3D ), 3, 3 * sizeof( QVector3D ) );
	AVertexAttribute* normalAttribute = new AVertexAttribute(
		QByteArrayLiteral( "normalAttr" ), GL_FLOAT, 2 * sizeof( QVector3D ), 3, 3 * sizeof( QVector3D ) );

	cube->setVertexAttribute( positionAttribute );
	cube->setVertexAttribute( colorAttribute );
	cube->setVertexAttribute( normalAttribute );

	setGeometry( cube );
}
