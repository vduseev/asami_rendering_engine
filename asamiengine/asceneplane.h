#ifndef ASCENEPLANE_H
#define ASCENEPLANE_H

#include "aobject3D.h"

class AScenePlane : public AObject3D
{
public:
	AScenePlane(void);
	~AScenePlane(void);

	void initialize();

private:
	void prepareGeometry();
	void prepareMaterial();
};

#endif

