#include "atexturedcube.h"

#include "amaterial.h"
#include "ageometry.h"
#include "avertexattribute.h"
#include "asimpletexturevertex.h"

#define v QVector2D

ATexturedCube::ATexturedCube()
	: AObject3D( false, false )
{
}

void ATexturedCube::initialize( const QString& textureFile, bool inverseNormals )
{
	prepareGeometry( inverseNormals );
	prepareMaterial( textureFile );
}

void ATexturedCube::update( float time )
{
}

void ATexturedCube::prepareMaterial( const QString& textureFile )
{
	AMaterial* material = new AMaterial();
	material->setShaders( "shaders/textured_object.vert", "shaders/textured_object.frag" );

	ASampler* tilingSampler = new ASampler();
    tilingSampler->create();
    tilingSampler->setMinificationFilter( GL_LINEAR_MIPMAP_LINEAR );
    tilingSampler->setSamplerParameterF( GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f );
    tilingSampler->setMagnificationFilter( GL_LINEAR );
	tilingSampler->setWrapMode( ASampler::DirectionS, GL_REPEAT );
    tilingSampler->setWrapMode( ASampler::DirectionT, GL_REPEAT );

	QImage image( textureFile );
    ATexture* texture = new ATexture();
    texture->create();
    texture->bind();
    texture->setImage( image );
    texture->generateMipMaps();
    material->setTextureUnit( 0, texture, tilingSampler, QByteArrayLiteral( "texture0" ) );

	setMaterial( material );
}

void ATexturedCube::prepareGeometry( bool inverseNormals )
{
	AGeometry* geometry = new AGeometry();
	geometry->setUsagePattern( QOpenGLBuffer::StaticDraw, QOpenGLBuffer::VertexBuffer );
	geometry->setUsagePattern( QOpenGLBuffer::StaticDraw, QOpenGLBuffer::IndexBuffer );

	// calculate Vertex Positions Of Cube
	QVector3D cube[ 8 ];
	for ( int i = 0; i < 8; i++ )
	{
		// each even set of 4 vertices has -0.5f z-coord
		float z = ( ( i / 4 ) % 2 == 0 ) ? -0.5f : 0.5f;
		// each even pair of vertices has 0.5f y-coord;
		float y = ( ( i / 2 ) % 2 == 0 ) ? 0.5f : -0.5f; 
		// each vertex with index 0 or 3 has -0.5f x-coord;
		float x = ( ( i % 4 == 0 ) || ( ( i - 3 ) % 4 == 0 ) ) ? -0.5f : 0.5f;

		cube[ i ] = QVector3D( x, y, z );
	}

	ASimpleTextureVertex* vertices = new ASimpleTextureVertex[ 4 * 6 ];
	
	float	a = 1.0f / 4.0f,
			m = 1.0f / 3.0f,
			d = 0.002f;

	// side 1									// side 2
	vertices[ 0 ].position	= cube[ 4 ];		vertices[ 4 ].position	= cube[ 0 ];
	vertices[ 0 ].texCoords	= v(0, 2*m - d);	vertices[ 4 ].texCoords	= v(a, 2*m);
	vertices[ 1 ].position	= cube[ 0 ];		vertices[ 5 ].position	= cube[ 1 ];
	vertices[ 1 ].texCoords	= v(a, 2*m - d);	vertices[ 5 ].texCoords	= v(2*a, 2*m);
	vertices[ 2 ].position	= cube[ 3 ];		vertices[ 6 ].position	= cube[ 2 ];
	vertices[ 2 ].texCoords	= v(a, m + d);		vertices[ 6 ].texCoords	= v(2*a, m);
	vertices[ 3 ].position	= cube[ 7 ];		vertices[ 7 ].position	= cube[ 3 ];
	vertices[ 3 ].texCoords	= v(0, m + d);		vertices[ 7 ].texCoords	= v(a, m);

	// side 3									// side 4
	vertices[ 8 ].position	= cube[ 1 ];		vertices[ 12 ].position	= cube[ 5 ];
	vertices[ 8 ].texCoords	= v(2*a, 2*m - d);	vertices[ 12 ].texCoords	= v(3*a, 2*m - d);
	vertices[ 9 ].position	= cube[ 5 ];		vertices[ 13 ].position	= cube[ 4 ];
	vertices[ 9 ].texCoords	= v(3*a, 2*m - d);	vertices[ 13 ].texCoords	= v(1.0f, 2*m - d);
	vertices[ 10 ].position	= cube[ 6 ];		vertices[ 14 ].position	= cube[ 7 ];
	vertices[ 10 ].texCoords= v(3*a, m + d);	vertices[ 14 ].texCoords	= v(1.0f, m + d);
	vertices[ 11 ].position	= cube[ 2 ];		vertices[ 15 ].position	= cube[ 6 ];
	vertices[ 11 ].texCoords= v(2*a, m + d);	vertices[ 15 ].texCoords	= v(3*a, m + d);

	// side 5									// side 6
	vertices[ 16 ].position	= cube[ 4 ];		vertices[ 20 ].position	= cube[ 3 ];
	vertices[ 16 ].texCoords= v(a + d, 1.0f);	vertices[ 20 ].texCoords	= v(a + d, m);
	vertices[ 17 ].position	= cube[ 5 ];		vertices[ 21 ].position	= cube[ 2 ];
	vertices[ 17 ].texCoords= v(2*a - d, 1.0f);	vertices[ 21 ].texCoords	= v(2*a - d, m);
	vertices[ 18 ].position	= cube[ 1 ];		vertices[ 22 ].position	= cube[ 6 ];
	vertices[ 18 ].texCoords= v(2*a - d, 2*m);	vertices[ 22 ].texCoords	= v(2*a - d, 0);
	vertices[ 19 ].position	= cube[ 0 ];		vertices[ 23 ].position	= cube[ 7 ];
	vertices[ 19 ].texCoords= v(a + d, 2*m);	vertices[ 23 ].texCoords	= v(a + d, 0);

		
	GLuint* indices = new GLuint[ 36 ];

	if ( inverseNormals )
	{
		GLuint mas[] = {
			// outside
			0, 1, 2,		0, 2, 3,	// side 1
			4, 5, 6,		4, 6, 7,	// side 2
			8, 9, 10,		8, 10, 11,	// side 3
			12, 13, 14,		12, 14, 15,	// side 4
			16, 17, 18,		16, 18, 19,	// side 5
			20, 21, 22,		20, 22, 23,	// side 6	
		};
		memcpy( indices, mas, 36 * sizeof( GLuint ) );
	}
	else
	{
		GLuint mas[] = {
			// outside
			0, 1, 2,		0, 2, 3,	// side 1
			4, 5, 6,		4, 6, 7,	// side 2
			8, 9, 10,		8, 10, 11,	// side 3
			12, 13, 14,		12, 14, 15,	// side 4
			16, 17, 18,		16, 18, 19,	// side 5
			20, 21, 22,		20, 22, 23,	// side 6	
		};
		memcpy( indices, mas, 36 * sizeof( GLuint ) );
	}	
	
	AGeometry::calculateNormals( vertices, 4 * 6, indices, 36 );
		
	int vertexSize = vertices->size();
	geometry->setVertices( vertices, 4 * 6 );
	geometry->setIndices( indices, 36 );

	AVertexAttribute* positionAttribute = new AVertexAttribute(
		QByteArrayLiteral( "positionAttribite" ), 
		GL_FLOAT, 0, 
		3, vertices->size() );

	AVertexAttribute* normalAttribute = new AVertexAttribute(
		QByteArrayLiteral( "normalAttribute" ), 
		GL_FLOAT, sizeof( QVector3D ), 
		3, vertices->size() );

	AVertexAttribute* textureCoordinateAttribute = new AVertexAttribute(
		QByteArrayLiteral( "textureCoordinateAttribute" ), 
		GL_FLOAT, 2 * sizeof( QVector3D ), 
		3, vertices->size() );

	geometry->setVertexAttribute( positionAttribute );
	geometry->setVertexAttribute( normalAttribute );
	geometry->setVertexAttribute( textureCoordinateAttribute );

	setGeometry( geometry );

	delete vertices, indices;
}
