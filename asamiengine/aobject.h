#ifndef AOBJECT_H
#define AOBJECT_H

#include <QOpenGLFunctions_4_0_Core>
#include <QList>

class AAbstractLight;

class AObject : public QObject, protected QOpenGLFunctions_4_0_Core
{
public:
	AObject();
	~AObject();

	virtual void initialize();
	virtual void update( float time );
	virtual void draw( QMatrix4x4 matrix, QMatrix4x4 worldMatrix, QList<AAbstractLight*> lights );

	bool isDrawable() const { return m_isDrawable; }
	bool isLight() const { return m_isLight; }
	
	void setDrawable( bool dr) { m_isDrawable = dr; }
	void setLight( bool lt ) { m_isLight = lt; }
	
	void setTime( float time ) { m_time = time; }
	float currentTime() const { return m_time; }

private:
	float m_time;
	bool m_isDrawable;
	bool m_isLight;
};

#endif

