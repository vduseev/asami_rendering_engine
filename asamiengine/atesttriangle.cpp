#include "atesttriangle.h"

#include "amaterial.h"
#include "ageometry.h"
#include "avertexattribute.h"
#include "asimpletexturevertex.h"

ATestTriangle::ATestTriangle()
	: AObject3D( false, false )
{
	prepareGeometry();
	prepareMaterial();
}

ATestTriangle::~ATestTriangle()
{
}

void ATestTriangle::prepareMaterial()
{
	AMaterial* material = new AMaterial();
	material->setShaders( "shaders/test.vert", "shaders/test.frag" );
	setMaterial( material );
}

void ATestTriangle::prepareGeometry()
{
	AGeometry* geometry = new AGeometry();
	geometry->setUsagePattern( QOpenGLBuffer::StaticDraw, QOpenGLBuffer::VertexBuffer );
	geometry->setUsagePattern( QOpenGLBuffer::StaticDraw, QOpenGLBuffer::IndexBuffer );
	
	QVector3D* vertices = new QVector3D[ 4 ];
	vertices[ 0 ] = QVector3D( -1.0f, 1.0f, 0.0f );
	vertices[ 1 ] = QVector3D( 1.0f, 1.0f, 0.0f );
	vertices[ 2 ] = QVector3D( 1.0f, -1.0f, 0.0f );
	vertices[ 3 ] = QVector3D( -1.0f, -1.0f, 0.0f );
	
	GLuint* indices = new GLuint[ 6 ];
	indices[ 0 ]	= 0;
	indices[ 1 ]	= 1;
	indices[ 2 ]	= 2;
	indices[ 3 ]	= 0;
	indices[ 4 ]	= 2;
	indices[ 5 ]	= 3;

	geometry->setVertices( vertices, 4 );
	geometry->setIndices( indices, 6 );

	AVertexAttribute* positionAttribute = new AVertexAttribute(
		QByteArrayLiteral( "positionAttribute" ), 
		GL_FLOAT, 0, 
		3, sizeof( QVector3D ) );

	geometry->setVertexAttribute( positionAttribute );

	setGeometry( geometry );
}
