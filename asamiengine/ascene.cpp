#include "ascene.h"

#include "abillboard.h"
#include "atexturedcube.h"
#include "acamera.h"
#include "anode.h"

AScene::AScene()
	: m_time( 0.0f )
	, m_metersToUnits( 0.05f )
	, m_cameraSpeed( 300.0f )
{
}

AScene::~AScene()
{
}

void AScene::initialize()
{
	m_camera = new ACamera( this );
	m_v = QVector3D();
    m_viewCenterFixed = false;
    m_panAngle = 0.0f;
    m_tiltAngle = 0.0f;

	// Initialize the camera position and orientation
    m_camera->setPosition( QVector3D( 15.0f, 15.0f, 15.0f ) );
    m_camera->setViewCenter( QVector3D( 0.0f, 0.0f, 0.0f ) );
    m_camera->setUpVector( QVector3D( 0.0f, 1.0f, 0.0f ) );

	// initialize 3D objects
	ABillboard* gridObject = new ABillboard( "textures/basicobjects/grid.png" );
	gridObject->scale( 100.0f );
	AAbstractScene::addObject3D( gridObject );

	ATexturedCube* skyboxObject = new ATexturedCube();
	skyboxObject->scale( 100.0f );
	skyboxObject->initialize( "textures/skybox/stormydays_large.jpg" );
	AAbstractScene::addObject3D( skyboxObject );
	
	ABillboard* circle = new ABillboard( "textures/basicobjects/blackwhitecircle.png" );
	AAbstractScene::addObject3D( circle );
	
	// initialize main node (the one and only child of m_objectTree)
	ANode* scene = new ANode();
	scene->translate( 0, 0, 0 );
	AAbstractScene::addNode( scene );

	// initialize rotating cubes
	ANode* rotationSurface = new ANode();
	rotationSurface->translate( 0, 3, 0 );
	m_rotationSurface = rotationSurface;
	AAbstractScene::addNode( rotationSurface );

	skyboxObject->setAssociatedNode( scene );
	gridObject->setAssociatedNode( scene );
	circle->setAssociatedNode( rotationSurface );
}

void AScene::update( float time )
{
	const float dt = time - m_time;
	m_time = time;

	// Update the camera position and orientation
    ACamera::CameraTranslationOption option = m_viewCenterFixed
                                           ? ACamera::DontTranslateViewCenter
                                           : ACamera::TranslateViewCenter;
    m_camera->translate( m_v * dt * m_metersToUnits, option );

	if ( !qFuzzyIsNull( m_panAngle ) )
    {
        m_camera->pan( m_panAngle, QVector3D( 0.0f, 1.0f, 0.0f ) );
        m_panAngle = 0.0f;
    }

    if ( !qFuzzyIsNull( m_tiltAngle ) )
    {
        m_camera->tilt( m_tiltAngle );
        m_tiltAngle = 0.0f;
    }

	// rotate
	// m_rotationSurface->rotate(dt * 50.0f, 0, 1, 0);

	AAbstractScene::update( time );
}

void AScene::render()
{
	// Pass in the usual transformation matrices
	AAbstractScene::setCameraViewMatrix( m_camera->viewMatrix() );
	AAbstractScene::setCameraProjectionMatrix( m_camera->projectionMatrix() );

	AAbstractScene::render();
}

void AScene::resize( int width, int height )
{
	float aspect = static_cast<float>( width ) / static_cast<float>( height );
    m_camera->setPerspectiveProjection( 45.0f, aspect, 0.01f, 3000.0f );
}

void AScene::keyPressEvent( Qt::Key key )
{
    switch ( key )
    {
		case Qt::Key_D:
            setSideSpeed( 1 );
            break;

        case Qt::Key_A:
            setSideSpeed( -1 );
            break;

        case Qt::Key_W:
            setForwardSpeed( 1 );
            break;

        case Qt::Key_S:
            setForwardSpeed( -1 );
            break;

        case Qt::Key_PageUp:
            setVerticalSpeed( 1 );
            break;

        case Qt::Key_PageDown:
            setVerticalSpeed( -1 );
            break;

        case Qt::Key_Shift:
            setViewCenterFixed( true );
            break;
    }
}

void AScene::keyReleaseEvent( Qt::Key key )
{
    switch ( key )
    {
		case Qt::Key_D:
        case Qt::Key_A:
            setSideSpeed( 0.0f );
            break;

        case Qt::Key_W:
        case Qt::Key_S:
            setForwardSpeed( 0.0f );
            break;

        case Qt::Key_PageUp:
        case Qt::Key_PageDown:
            setVerticalSpeed( 0.0f );
            break;

        case Qt::Key_Shift:
            setViewCenterFixed( false );
            break;
    }
}

void AScene::mousePressEvent( QMouseEvent* e )
{
    if ( e->button() == Qt::LeftButton )
    {
        m_leftButtonPressed = true;
        m_pos = m_prevPos = e->pos();
    }
}

void AScene::mouseReleaseEvent( QMouseEvent* e )
{
    if ( e->button() == Qt::LeftButton )
        m_leftButtonPressed = false;
}

void AScene::mouseMoveEvent( QMouseEvent* e )
{
    if ( m_leftButtonPressed )
    {
        m_pos = e->pos();
        float dx = 0.2f * ( m_pos.x() - m_prevPos.x() );
        float dy = -0.2f * ( m_pos.y() - m_prevPos.y() );
        m_prevPos = m_pos;

		pan( dx );
        tilt( dy );
    }
}