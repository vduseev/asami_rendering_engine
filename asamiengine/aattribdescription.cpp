#include "aattribdescription.h"

AAttribDescription::AAttribDescription(void)
{
}

AAttribDescription::AAttribDescription( const QByteArray& name, GLenum type, int offset, int tupleSize, int stride )
	: m_name( name )
	, m_type( type )
	, m_offset( offset )
	, m_tupleSize( tupleSize )
	, m_stride( stride )
{
}

AAttribDescription::~AAttribDescription(void)
{
}
