#include "asceneplane.h"

#include "amaterial.h"
#include "ageometry.h"
#include "avertexattribute.h"
#include "asimpletexturevertex.h"

AScenePlane::AScenePlane(void)
{
	m_isLightable = false;
}

AScenePlane::~AScenePlane(void)
{
}

void AScenePlane::initialize()
{
	AObject3D::initialize();

	prepareGeometry();
	prepareMaterial();
}

void AScenePlane::prepareMaterial()
{
	AMaterial* material = new AMaterial();
	material->setShaders( "shaders/sceneplane.vert", "shaders/sceneplane.frag" );

	ASampler* tilingSampler = new ASampler();
    tilingSampler->create();
    tilingSampler->setMinificationFilter( GL_LINEAR_MIPMAP_LINEAR );
    tilingSampler->setSamplerParameterF( GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f );
    tilingSampler->setMagnificationFilter( GL_NEAREST );
	tilingSampler->setWrapMode( ASampler::DirectionS, GL_REPEAT );
    tilingSampler->setWrapMode( ASampler::DirectionT, GL_REPEAT );

	QImage scenePlaneImage( "textures/sceneplane.png" );
    ATexture* scenePlaneTexture = new ATexture();
    scenePlaneTexture->create();
    scenePlaneTexture->bind();
    scenePlaneTexture->setImage( scenePlaneImage );
    scenePlaneTexture->generateMipMaps();
    material->setTextureUnit( 1, scenePlaneTexture, tilingSampler, QByteArrayLiteral( "planeTexture" ) );

	setMaterial( material );
}

void AScenePlane::prepareGeometry()
{
	AGeometry* plane = new AGeometry();

	plane->initialize();

	plane->setUsagePattern( QOpenGLBuffer::StaticDraw, QOpenGLBuffer::VertexBuffer );
	plane->setUsagePattern( QOpenGLBuffer::StaticDraw, QOpenGLBuffer::IndexBuffer );
	
	const int sideCount = 15;
	ASimpleTextureVertex vertices[ sideCount * sideCount ];

	float dx_vert = 2.0f / ( sideCount - 1 );

	for ( int i = 0; i < sideCount; i++ )
	{
		for ( int j = 0; j < sideCount; j++ )
		{
			vertices[ i * sideCount + j ].coordinate 
				= QVector3D( -1.0f + j * dx_vert, 0.0f, 1.0f - i * dx_vert );
			vertices[ i * sideCount + j ].texCoords
				= QVector2D( j, i );
		}
	}

	GLuint indices[ ( sideCount - 1) * ( sideCount - 1 ) * 6 ];

	int idx = 0;
	for ( int i = 0; i < sideCount - 1; i++ )
	{
		for ( int j = 0; j < sideCount - 1; j++ )
		{
			indices[ idx++ ]	= i * sideCount + j;
			indices[ idx++ ]	= i * sideCount + ( j + 1 );
			indices[ idx++ ]	= ( i + 1 ) * sideCount + ( j + 1 );
			indices[ idx++ ]	= i * sideCount + j;
			indices[ idx++ ]	= ( i + 1 ) * sideCount + ( j + 1 );
			indices[ idx++ ]	= ( i + 1 ) * sideCount + j;
		}
	}

	plane->setVertices( 
		vertices, 
		sizeof( QVector3D ) + sizeof( QVector2D ), 
		sideCount * sideCount );
	plane->setIndices(
		indices,
		( sideCount - 1 ) * ( sideCount - 1 ) * 6 );

	AVertexAttribute* positionAttribute = new AVertexAttribute(
		QByteArrayLiteral( "posAttr" ), GL_FLOAT, 
		0, 3, sizeof( QVector3D ) + sizeof( QVector2D ) );
	AVertexAttribute* textureCoordAttribute = new AVertexAttribute(
		QByteArrayLiteral( "texCoordAttr" ), GL_FLOAT, 
		sizeof( QVector3D ), 3, sizeof( QVector3D ) + sizeof( QVector2D ) );

	plane->setVertexAttribute( positionAttribute );
	plane->setVertexAttribute( textureCoordAttribute );

	setGeometry( plane );
}
