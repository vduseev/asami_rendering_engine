#ifndef ATESTTRIANGLE_H
#define ATESTTRIANGLE_H

#include "aobject3D.h"

#include <QString>

class ATestTriangle : public AObject3D
{
public:
	ATestTriangle();
	~ATestTriangle();

private:
	void prepareGeometry();
	void prepareMaterial();
};

#endif

