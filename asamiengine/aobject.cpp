#include "aobject.h"

#include "aabstractlight.h"

#include <QMatrix4x4>

AObject::AObject(void)
{
}

AObject::~AObject(void)
{
}

void AObject::initialize()
{
}

void AObject::update( float time )
{
}

void AObject::draw( QMatrix4x4 matrix, QMatrix4x4 worldMatrix, QList<AAbstractLight*> lights )
{
}
