#ifndef ACUBE_H
#define ACUBE_H

#include "aobject3D.h"

class ACube : public AObject3D
{
public:
	ACube();

	void initialize();

private:
	void prepareGeometry();
	void prepareMaterial();
};

#endif
