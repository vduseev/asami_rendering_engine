// Input
varying lowp vec2 f_texCoords;

// Uniform input
uniform sampler2D texture0;

void main() {

	vec4 fragColor = texture( texture0, texCoords );
	gl_FragColor = fragColor;
}