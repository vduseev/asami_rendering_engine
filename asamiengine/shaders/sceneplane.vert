attribute	highp	vec3 posAttr;
attribute	lowp	vec2 texCoordAttr;

varying		lowp	vec2 texCoord;

uniform		highp	mat4 matrix;
uniform		highp	mat4 worldMatrix;

void main() {

    texCoord = texCoordAttr;
	
	vec4 pos = vec4( posAttr.xyz, 1.0 );
	
	gl_Position = matrix * pos;
}