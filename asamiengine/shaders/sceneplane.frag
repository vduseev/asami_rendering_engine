varying lowp vec2 texCoord;

uniform sampler2D planeTexture;

void main() {

	vec4 fragColor = texture( planeTexture, texCoord );
	fragColor.a = 0.5;
		
	gl_FragColor = fragColor;

}