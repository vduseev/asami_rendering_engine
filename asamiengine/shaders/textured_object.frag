// Input
varying lowp vec3 normal;
varying lowp vec2 textureCoordinate;

// Uniform input
uniform sampler2D texture0;

void main() {

	vec4 fragColor = texture( texture0, textureCoordinate );
	//vec4 fragColor = vec4( 1.0, 1.0, 0.0, 1.0 );
	gl_FragColor = fragColor;
}