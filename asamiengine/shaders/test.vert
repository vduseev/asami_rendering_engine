// Input
attribute	highp	vec3 v_pos;
attribute	highp	vec2 v_texCoords;

// Output
varying lowp vec2 f_texCoords;

// Uniform input
uniform highp mat4 finalMatrix;

void main() {
	
	f_texCoords = v_texCoords;

	vec4 pos = vec4( v_pos.xyz, 1.0 );
	gl_Position = finalMatrix * pos;
}