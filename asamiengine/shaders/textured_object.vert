// Input
attribute	highp	vec3 positionAttribute;
attribute	highp	vec3 normalAttribute;
attribute	highp	vec2 textureCoordinateAttribute;

// Output
varying		lowp	vec3 normal;
varying		lowp	vec2 textureCoordinate;

// Uniform input
uniform		highp	mat4 worldMatrix;
uniform		highp	mat4 finalMatrix;

void main() {

    textureCoordinate	= textureCoordinateAttribute;
	normal				= normalAttribute;
	
	vec4 pos = vec4( positionAttribute.xyz, 1.0 );
	gl_Position = finalMatrix * pos;
}