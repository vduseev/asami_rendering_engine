varying lowp vec4 color;
varying lowp vec3 normal;

struct DirectionalLight
{
	vec3 Color;
	float AmbientIntensity;
	float DiffuseIntensity;
	vec3 Direction;
};

uniform DirectionalLight dirLight;

void main() {

	vec4 ambientColor = vec4( dirLight.Color, 1.0f ) * dirLight.AmbientIntensity; 
	float diffuseFactor = dot( normalize( normal ), -dirLight.Direction );

	vec4 diffuseColor = vec4( 1.0, 0.0, 0.0, 1.0 );

	if ( diffuseFactor > 0 ) 
	{
		diffuseColor = vec4( dirLight.Color, 1.0f ) * dirLight.DiffuseIntensity * diffuseFactor;
	}
	else
	{
		diffuseColor = vec4( 0, 0, 0, 1 );
	}

	gl_FragColor = color * ( ambientColor + diffuseColor);

}