// input
attribute	highp	vec3 positionAttr;
attribute	lowp	vec3 colorAttr;
attribute	lowp	vec3 normalAttr;

// output
varying		lowp	vec4 color;
varying		lowp	vec3 normal;

// uniform
uniform		highp	mat4 matrix;
uniform		highp	mat4 worldMatrix;

void main() {

    color = vec4( colorAttr.xyz, 1.0 );
	
	vec4 pos = vec4( positionAttr.xyz, 1.0 );

	vec4 n = worldMatrix * vec4( normalAttr.xyz, 0.0 );

	normal = n.xyz;
	
	gl_Position = matrix * pos;
}